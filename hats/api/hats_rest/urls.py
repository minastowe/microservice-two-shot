from django.urls import path
from .views import api_list_hats, api_show_hats

urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_show_hats, name="api_show_hats"),  
]



# FROM URLS.PY WARDROBE
# from django.urls import path
# from .views import api_location, api_locations, api_bin, api_bins


# urlpatterns = [
#     path("locations/", api_locations, name="api_locations"),
#     path("locations/<int:pk>/", api_location, name="api_location"),
#     path("bins/", api_bins, name="api_bins"),
#     path("bins/<int:pk>/", api_bin, name="api_bin"),
# ]