import { useState, useEffect } from "react";

function ShoeList(props) {
    const [shoes, setShoes] = useState([]);

    async function fetchShoes() {
        const response = await fetch(('http://localhost:8080/api/shoes/'))

        if (response.ok){
            const parsedJson = await response.json();
            setShoes(parsedJson.shoes);
        }
    }

    useEffect(() => {
        fetchShoes();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Closet Name</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.href}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.bin.closet_name}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}
export default ShoeList;
