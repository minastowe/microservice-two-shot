import React, {useState, useEffect} from 'react';

function ShoeForm () {
  const [bins, setBins] = useState([])

  const [manufacturer, setManufacturer] = useState("")
  const [modelName, setModelName] = useState("")
  const [color, setColor] = useState("")
  const [urlPic, setUrlPic] = useState("")
  const [bin, setBin] = useState("")

  const getData = async () => {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  useEffect(()=> {
    getData();
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.manufacturer = manufacturer;
    data.model_name = modelName;
    data.color = color;
    data.url = urlPic;
    data.bin = bin;

    const url = 'http://localhost:8080/api/shoes/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setManufacturer("");
      setModelName("");
      setColor("");
      setUrlPic("");
      setBin("");
    }
  }

  const handleManufacturerChange = (e) => {
    setManufacturer(e.target.value);
  }

  const handleModelNameChange = (e) => {
    setModelName(e.target.value);
  }

  const handleColorChange = (e) => {
    setColor(e.target.value);
  }

  const handleUrlPicChange = (e) => {
    setUrlPic(e.target.value);
  }

  const handleBinChange = (e) => {
    setBin(e.target.value)
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new Shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required name="manufacturer" type="text" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleModelNameChange} value={modelName} placeholder="Model Name" required name="model_name" type="text" id="model_name" className="form-control" />
              <label htmlFor="model_name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} value={color} placeholder="Color" type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleUrlPicChange} value={urlPic} placeholder="Url Pic" required name="url" type="url" id="url_pic" className="form-control" />
              <label htmlFor="url_pic">Picture URL</label>
            </div>
            <div className="mb-3">
              <select onChange={handleBinChange} value={bin} required name="bin" className="form-select" id="bin">
                <option value="">Choose a Bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
