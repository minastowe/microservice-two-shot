import React, {useState, useEffect} from 'react';

function HatForm () {
  const [locations, setLocations] = useState([])

  const [fabric, setFabric] = useState("")
  const [styleName, setStyleName] = useState("")
  const [color, setColor] = useState("")
  const [urlPic, setUrlPic] = useState("")
  const [location, setLocation] = useState("")
  

  const getData = async () => {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data)
      setLocations(data.locations);
      console.log(locations)
    }
  }


  useEffect(()=> {
    getData();
  }, [])

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.fabric = fabric;
    data.style_name = styleName;
    data.color = color;
    data.url_pic = urlPic;
    data.location = location;

    // const url = 'http://localhost:8090/api/hats/';
    const url = `http://localhost:8090/api/hats/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);
    
    if (response.ok) {
      setFabric("");
      setStyleName("");
      setColor("");
      setUrlPic("");
      setLocation("");
    }
  }

  const handleFabricChange = (e) => {
    setFabric(e.target.value);
  }

  const handleStyleNameChange = (e) => {
    setStyleName(e.target.value);
  }

  const handleColorChange = (e) => {
    setColor(e.target.value);
  }

  const handleUrlPicChange = (e) => {
    setUrlPic(e.target.value);
  }

  const handleLocationChange = (e) => {
    setLocation(e.target.value)
  }
  
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required name="fabric" type="text" id="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStyleNameChange} value={styleName} placeholder="styleName" required name="styleName" type="text" id="styleName" className="form-control" />
              <label htmlFor="styleName">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} value={color} placeholder="color" type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleUrlPicChange} value={urlPic} placeholder="urlPic" required name="urlPic" type="text" id="urlPic" className="form-control" />
              <label htmlFor="urlPic">Url Pic</label>
            </div>
            <div className="mb-3">
              <select onChange={handleLocationChange} value={location} required name="location" className="form-select" id="location">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.href} value={location.href}>{location.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
