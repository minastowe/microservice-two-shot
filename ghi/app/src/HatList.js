import {useState, useEffect } from 'react';

function HatsList(){

    const [hats, setHats] = useState([]);

    async function fetchHats() {
        const response = await fetch('http://localhost:8090/api/hats/');

        if (response.ok){
            const parsedJson = await response.json();
            setHats(parsedJson.hats);
        }
    }

    useEffect(() => {
        fetchHats();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Closet Name</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.href}>
                            <td>{ hat.fabric}</td>
                            <td>{ hat.style_name}</td>
                            <td>{ hat.color}</td>
                            <td>{ hat.location.closet_name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}
export default HatsList;