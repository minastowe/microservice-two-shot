from django.contrib import admin
from .models import Shoe, BinVO

# Register your models here.
@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    list_display = [
        "manufacturer",
        "model_name",
        "color",
        "url",
        "bin"
    ]

@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    list_display = [
        "closet_name",
        "bin_number",
        "bin_size"
    ]
